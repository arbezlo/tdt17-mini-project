This project aims to deliver a functionning object detection algorithm for use in Lidar based imaging applied to autonomous driving.

It was mainly realised through yolov5, more precisely yolov5s transfer learning. So you will find the ultralytics\yolov5 GitHub repository attached. A few test were made trhough the mikel-bromstom\Yolov5_DeepSort_Pytorch tracking library but none were exploited in the end.

You will find in the folder Results_transfer most of the different models performances and weights.

The python script Video_to_images.py is used to convert the videos provided to images in order to use them efficiently in the learning phase.

The python script Evaluation_yolov5.py is an unfinished attempt at making an evaluation program for yolov5.

Most of the learning process has been done through this colab notebook provided by the yolov5 team :
https://colab.research.google.com/github/roboflow-ai/yolov5-custom-training-tutorial/blob/main/yolov5-custom-training.ipynb

Most of the preprocessing has been done through roboflow as indicated in the notebook.

