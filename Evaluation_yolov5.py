import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision
import matplotlib.pyplot as plt
import os
import skimage as ski
import pandas as pd
import cv2
import sklearn as skl


model = torch.hub.load('ultralytics/yolov5', 'custom', path = 'Results_transfert/exp_0111203/weights/best_0111203.pt')

def retriveLabelsCoord(frame_number):

    if( frame_number<10 ):
        path = 'Videos/Video00002_Ano/02_yolo/obj_train_data/frame_00000' + str(frame_number) + '.txt'
    else:
        path = 'Videos/Video00002_Ano/02_yolo/obj_train_data/frame_0000' + str(frame_number) + '.txt'
    with open(path) as f:
        real = f.readlines()
        real = [k.rsplit() for k in real]
        f.close()
    return [int(k[0]) for k in real], [[float(real[l][j]) for j in range(1,len(real[0]))] for l in range(len(real))]


def dist(x1,x2):
    return np.dot(np.transpose(np.array(x1,x2)),np.array(x1,x2))

for i in range(1):
    img = cv2.imread('Images/Video00002/'+str(i)+'.jpg')
    prediction = model(img)
    pred_labels, pred_cord_thres = prediction.xyxyn[0][:, -1].numpy(), prediction.xyxyn[0][:, :-1].numpy()
    pred_cord, pred_thres = pred_cord_thres[0:3], pred_cord_thres[4]
    real_labels, real_cord = retriveLabelsCoord(i)
    print(f'real_labels : {real_labels}, real_cord: {real_cord}\n')
    print(f'frame number : {i}\t')
    print(f'labels : {pred_labels}\t')
    print(f'cord : {pred_cord}, thres: {pred_thres}\n')


