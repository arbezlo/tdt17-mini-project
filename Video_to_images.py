import sys
import cv2
import numpy as np

import imutils

c = 0


def extractImages(pathIn, pathOut, c):
    cap = cv2.VideoCapture(pathIn)
    if cap.isOpened():
        current_frame = 0
        while current_frame<100:
            ret, frame = cap.read()
            if ret:
                if current_frame<10:
                    name = pathOut + "0" + str(current_frame) + ".jpg"
                else:
                    name = pathOut + str(current_frame) + ".jpg"
                print(f"Creating file... {name}")
                cv2.imwrite(name, frame)
            current_frame += 1
        cap.release()
    return c


for i in range(19):
    c = 0
    if i < 10:
        if i != 5:
            pathIn = "./Videos/Video0000" + str(i) + "_Ano/Video0000" + str(i) + "_intensity.avi"
            pathOut = "./yolov5/LiDAR_img/images/train/0"+str(i)+"_yolo/frame_0000"
            c = extractImages(pathIn, pathOut, c)
            print(c)
    else:
        if i not in  [15,11,12]:
            pathIn = "./Videos/Video000" + str(i) + "_Ano/Video000" + str(i) + "_intensity.avi"
            pathOut = "./yolov5/LiDAR_img/images/train/"+str(i)+"_yolo/frame_0000"
            c = extractImages(pathIn, pathOut, c)
            print(c)
        else:
            pathIn = "./Videos/Video000" + str(i) + "_Ano/Video000" + str(i) + "_intensity.avi"
            pathOut = "./yolov5/LiDAR_img/images/valid/"+str(i)+"_yolo/frame_0000"
            c = extractImages(pathIn, pathOut, c)
            print(c)
    print("Video"+str(i)+" succesfully Transformed\n")
print("Finished")

